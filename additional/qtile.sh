#!/usr/bin/env bash


echo "enter username:"
read USER

## Script for setting up a customized Qtile WM on any Ubuntu/Debian based System

sudo apt install -y python3 
sudo apt install -y python3-pip libxcb-render0-dev libffi-dev
     pip3 install xcffib
sudo apt install libcairo2
     pip3 install --no-cache-dir cairocffi
sudo apt install -y libpangocairo-1.0-0 python-dbus
     pip3 install qtile
sudo touch /usr/share/xsessions/qtile.desktop
     echo "[Desktop Entry]"       | sudo tee -a /usr/share/xsessions/qtile.desktop
     echo "Name=Qtile"            | sudo tee -a /usr/share/xsessions/qtile.desktop
     echo "Comment=Qtile Session" | sudo tee -a /usr/share/xsessions/qtile.desktop
     echo "Exec=qtile"            | sudo tee -a /usr/share/xsessions/qtile.desktop
     echo "Type=Application"      | sudo tee -a /usr/share/xsessions/qtile.desktop
     echo "Keywords=wm;tiling"    | sudo tee -a /usr/share/xsessions/qtile.desktop

##### Neccessary Application and Libraries for Qtile

sudo apt install -y lxapperance variety pcmanfm htop dmenu xfce4-power-manager compton keepassxc evince iwd flatpak tlp powertop fail2ban lutris grub-customizer thunderbird celluloid geany geany-plugins solaar hddtemp lm-sensors psensor flameshot smartmontools flatpak

sudo apt install vim terminator wget curl unrar unzip rsync ntp neofetch bleachbit

flatpak install flathub org.onlyoffice.desktopeditors
pip3 install bpytop --upgrade

##### ZSH install
sudo apt install -y zsh zsh-completions zsh-autosuggestions zsh-syntax-highlighting autojump
git clone https://gitlab.com/stefanleis1/autoinstall/arch.git
cp /home/${USER}/apt/additional/arch/.zshrc /home/${USER}/.zshrc
bash /home/${USER}/apt/additional/arch/additional_postinstallscripts/13_customize_zsh.sh

# Till the date of publication of this article, the latest available download version is the 0.8.0
wget -c https://github.com/ogham/exa/releases/download/v0.8.0/exa-linux-x86_64-0.8.0.zip
unzip exa-linux-x86_64-0.8.0.zip
# Move the unziped binary with the name "exa-linux-x86_64" to "/usr/local/bin/" with the exa name
sudo mv exa-linux-x86_64 /usr/local/bin/exa
## Qtile config customization

git clone https://gitlab.com/stefanleis1/data/dotfiles
cp -r ~/dotfiles/.config/qtile ~/.config

# Auto log off 
sudo pkill -u ${USER}
