#!/usr/bin/env bash

#################### Setting Up Some Basic Variables ###################################
lsblk -f
read -p "Please enter disk [/dev/sda]" DISK
DISK=${DISK:-/dev/sda}
read -p "Please enter desired username [stefan]:" USER
USER=${USER:-stefan}
echo "Please enter computerame:"
read COMPUTERNAME
echo -e "\nUser password configuration:\n$HR"
useradd -m ${USER}
passwd ${USER}
usermod -aG users,wheel,audio,video,optical,storage ${USER}
mkdir /home/${USER}/Bilder
mkdir /home/${USER}/Musik
mkdir /home/${USER}/Videos
mkdir /home/${USER}/Dokumente
mkdir /home/${USER}/Downloads
chown ${USER}:${USER} /home/${USER}/Bilder
chown ${USER}:${USER} /home/${USER}/Musik
chown ${USER}:${USER} /home/${USER}/Videos
chown ${USER}:${USER} /home/${USER}/Dokumente
chown ${USER}:${USER} /home/${USER}/Downloads
sed -i 's/^# %wheel ALL=(ALL) NOPASSWD: ALL/%wheel ALL=(ALL) NOPASSWD: ALL/' /etc/sudoers
##########################################
apt install grub dosfstools os-prober mtools efibootmgr cryptsetup ntfs-3g netctl -y
echo -e "\nWriting EncryptionKeys To LUKS Headers:\n$HR"
dd bs=512 count=4 if=/dev/random of=/etc/key iflag=fullblock
chmod 600 /etc/key
cryptsetup -v luksAddKey "${DISK}p2" /etc/key && cryptsetup -v luksAddKey "${DISK}p3" /etc/key
echo "luks-btrfs-root   UUID=$(blkid -o value -s UUID ${DISK}p3)        /etc/key        luks" >> /etc/crypttab

echo "KEYFILE_PATTERN=/etc/key" >> /etc/cryptsetup-initramfs/conf-hook
echo "UMASK=0077" >> /etc/initramfs-tools/initramfs.conf
####################### Bootloader Installation and Configuration ######################################>
echo 'GRUB_CMDLINE_LINUX="quiet cryptdevice=%uuid3%:luks-btrfs-root root=/dev/mapper/luks-btrfs-root cryptdevice=UUID=%uuid2%:luks-btrfs-boot cryptkey=/etc/key"' >> /etc/default/grub
sed -i s/%uuid2%/$(blkid -o value -s UUID ${DISK}p2)/ /etc/default/grub
sed -i s/%uuid3%/$(blkid -o value -s UUID ${DISK}p3)/ /etc/default/grub
echo "GRUB_ENABLE_CRYPTODISK=y" >> /etc/default/grub
grub-install --target=x86_64-efi --bootloader-id=CRYPTDEB
update-initramfs -u -k all
###
nano /etc/default/grub
