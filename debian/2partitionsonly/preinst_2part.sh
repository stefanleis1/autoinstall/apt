
#!/usr/bin/env bash

echo "This is the configuration script for Debian on a NVME-SSD"
sudo apt update

echo -e "\nInstalling prereqs...\n$HR"
sudo apt install debootstrap -y

echo "-------------------------------------------------"
echo "-------select your disk to format----------------"
echo "-------------------------------------------------"
lsblk -f
read -p "Please enter disk: (default: /dev/sda)" DISK
DISK=${DISK:-/dev/sda}
echo "--------------------------------------"
echo -e "\nFormating disk...\n$HR"
echo "--------------------------------------"
#################### Disk Prep ###########################################################
sgdisk -Z ${DISK} # zap all on disk
sgdisk -a 2048 -o ${DISK} # new gpt disk 2048 alignment
#################### Create Partitions ###################################################
sgdisk -n 1:0:+500M ${DISK} # partition 1 (UEFI SYS), default start block, 512MB
sgdisk -n 2:0:0     ${DISK} # partition 3 (LVM), with root and home partition later on
#################### Set Partition Types #################################################
sgdisk -t 1:ef00 ${DISK}
sgdisk -t 2:8300 ${DISK}
#################### Label Partitions ####################################################
sgdisk -c 1:"UEFISYS" ${DISK}
sgdisk -c 2:"BTRFS"   ${DISK}
#################### LUKS encryption ###################################################### 
cryptsetup luksFormat -v --key-size 512 --hash sha256 --iter-time 2000 --use-random ${DISK}p2 
cryptsetup open ${DISK}p2 cryptroot
mkfs.vfat -F32 -n "UEFISYS" ${DISK}p1
mkfs.btrfs /dev/mapper/cryptroot
################### Mount Target && BTRFS Setup  ############################################################
mount /dev/mapper/cryptroot /mnt
cd /mnt
btrfs subvolume create @
btrfs subvolume create @home
btrfs subvolume create @snapshots
btrfs subvolume create @var
cd
umount /mnt
mount -o noatime,compress=zstd,space_cache=v2,ssd,discard=async,subvol=@ /dev/mapper/cryptroot /mnt
mkdir -p /mnt/{boot,home,var,.snapshots}
mount -o noatime,compress=zstd,space_cache=v2,discard=async,subvol=@home /dev/mapper/cryptroot /mnt/home
mount -o noatime,compress=zstd,space_cache=v2,discard=async,subvol=@snapshots /dev/mapper/cryptroot /mnt/.snapshots
mount -o noatime,compress=zstd,space_cache=v2,discard=async,subvol=@var /dev/mapper/cryptroot /mnt/var
mkdir /mnt/boot/efi
mount ${DISK}1p /mnt/boot/efi
mkdir /mnt/etc
#################################
debootstrap --include linux-image-amd64,grub-efi,locales --arch amd64 bullseye /mnt
mount -o bind /dev /mnt/dev
mount -o bind /dev/pts /mnt/dev/pts
mount -o bind /proc /mnt/proc
mount -o bind /sys /mnt/sys
sudo apt install arch-install-scripts
# ################### Generating Fstab-mount Directory #########################################

cp /etc/mtab /mnt/etc
genfstab -U /mnt >> /mnt/etc/fstab
# ################### Installing Some Needed Basic Packages For Further Installation ###########
#pacstrap /mnt base base-devel linux-lts linux-lts-headers linux-lts-docs linux-firmware vim nano sudo git btrfs-progs --noconfirm --needed 
chroot /mnt



