
#!/usr/bin/env bash

#################### Setting Up Some Basic Variables ###################################
lsblk -f
read -p "Please enter disk [/dev/sda]" DISK
DISK=${DISK:-/dev/sda}
read -p "Please enter desired username [stefan]:" USER
USER=${USER:-stefan}
read -p "Please enter computerame:" COMPUTERNAME
COMPUTERNAME=${COMPUTERNAME:-mainsys}
echo -e "\nUser password configuration:\n$HR"
apt install firmware-linux firmware-linux-nonfree sudo vim git wget ca-certificates -y
echo "deb https://deb.debian.org/debian bullseye main contrib non-free" | tee -a/etc/apt/sources.list
echo "deb https://security.debian.org/debian-security bullseye-security main contrib non-free" | tee -a /etc/apt/sources.list
echo "deb https://deb.debian.org/debian bullseye-updates main contrib non-free" | tee -a /etc/apt/sources.list
apt update
dpkg-reconfigure tzdata
dpkg-reconfigure locales
echo "$COMPUTENAME" > /etc/hostname
echo "127.0.1.1    $COMPUTENAME.localdomain $COMPUTENAME" | tee -a /etc/hosts
echo "auto lo" | tee -a /etc/network/interfaces
echo "iface lo inet loopback" | tee -a /etc/network/interfaces
echo "auto eth0" | tee -a /etc/network/interfaces
echo "iface eth0 inet dhcp" | tee -a /etc/network/interfaces
apt install dhcpcd5 network-manager wireless-tools wpasupplicant dialog -y
useradd -m ${USER}
passwd ${USER}
#usermod -aG users,wheel,audio,video,optical,storage ${USER}
mkdir /home/${USER}/Bilder
mkdir /home/${USER}/Musik
mkdir /home/${USER}/Videos
mkdir /home/${USER}/Dokumente
mkdir /home/${USER}/Downloads
chown ${USER}:${USER} /home/${USER}/Bilder
chown ${USER}:${USER} /home/${USER}/Musik
chown ${USER}:${USER} /home/${USER}/Videos
chown ${USER}:${USER} /home/${USER}/Dokumente
chown ${USER}:${USER} /home/${USER}/Downloads
sed -i 's/^# %wheel ALL=(ALL) NOPASSWD: ALL/%wheel ALL=(ALL) NOPASSWD: ALL/' /etc/sudoers
##########################################
apt install grub2 dosfstools os-prober mtools efibootmgr cryptsetup ntfs-3g netctl -y
#echo -e "\nWriting EncryptionKeys To LUKS Headers:\n$HR"
#dd bs=512 count=4 if=/dev/random of=/etc/key iflag=fullblock
#chmod 600 /etc/key
#cryptsetup -v luksAddKey "${DISK}p2" /etc/key && cryptsetup -v luksAddKey "${DISK}p3" /etc/key
echo "cryptroot  UUID=$(blkid -o value -s UUID ${DISK}p2)        none        luks" | tee -a /etc/crypttab

#echo "KEYFILE_PATTERN=/etc/key" | tee -a /etc/cryptsetup-initramfs/conf-hook
echo "UMASK=0077" | tee -a /etc/initramfs-tools/initramfs.conf
####################### Bootloader Installation and Configuration ######################################>
#echo 'GRUB_CMDLINE_LINUX="quiet cryptdevice=%uuid3%:cryptroot root=/dev/mapper/cryptroot"' | tee -a /etc/default/grub
#sed -i s/%uuid2%/$(blkid -o value -s UUID ${DISK}p2)/ /etc/default/grub
sed -i s/%uuid3%/$(blkid -o value -s UUID ${DISKp}2)/ /etc/default/grub
echo "GRUB_ENABLE_CRYPTODISK=y" | tee -a /etc/default/grub
grub-install --target=x86_64-efi --bootloader-id=CRYPTDEB
update-initramfs -u -k all
###
nano /etc/default/grub
