#!/usr/bin/env bash
sudo apt update && sudo apt full-upgrade -y
sudo apt install git python3-pip neofetch zsh zsh-syntax-highlighting autojump zsh-autosuggestions hddtemp lm-sensors tldr ufw fail2ban vim vim-doc libssl-dev -y
read -p "enter User:" USER
# Customizing Terminalprompt # 
touch "$HOME/.cache/zshhistory"
#git clone --depth=1 https://gitlab.com/stefanleis1/autoinstall/qtile.git ~/qtile
git clone --depth=1 https://github.com/romkatv/powerlevel10k.git ~/powerlevel10k
echo 'source ~/powerlevel10k/powerlevel10k.zsh-theme' >> ~/.zshrc
sudo  usermod -s /bin/zsh ${USER}
mv /home/${USER}/.zshrc /home/${USER}/.zshrc.latest
cp /home/${USER}/dotfiles/.zshrc /home/${USER}/.zshrc
cp /home/${USER}/dotfiles/.bashrc /home/${USER}/.bashrc
cp /home/${USER}/dotfiles/.config_current/zsh /home/${USER}/.config
curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
# network config
echo "interface eth0"                                   | sudo tee -a /etc/dhcpcd.conf
echo "static ip_address=192.168.188.15/24"               | sudo tee -a /etc/dhcpcd.conf
echo "static routers=192.168.137.15"                     | sudo tee -a /etc/dhcpcd.conf
#echo "static domain_name_servers=1.1.1.1"               | sudo tee -a /etc/dhcpcd.conf

echo "Port=422" | sudo tee -a /etc/ssh/sshd_config
echo "Port=422" | sudo tee -a /etc/ssh/ssh_config
################# Harden Linux ################################################
# check for unsigned kernel modules
for mod in $(lsmod | tail -n +2 | cut -d' ' -f1); do modinfo ${mod} | grep -q "signature" || echo "no signature for module: ${mod}" ; done

#-----------------------
#--Required Packages-
#-ufw
#-fail2ban

#-start firewall
sudo systemctl enable ufw
# --- Setup UFW rules
sudo ufw limit 22/tcp  
sudo ufw allow 80/tcp  
sudo ufw allow 443/tcp  
sudo ufw allow 422/tcp
sudo ufw default deny incoming  
sudo ufw default allow outgoing
sudo ufw enable

# --- Harden /etc/sysctl.conf
#sudo sysctl kernel.modules_disabled=1
sudo sysctl -a
sudo sysctl -A
sudo sysctl mib
sudo sysctl net.ipv4.conf.all.rp_filter
sudo sysctl -a --pattern 'net.ipv4.conf.(eth|wlan)0.arp'

# --- PREVENT IP SPOOFS
cat <<EOF > /etc/host.conf
order bind,hosts
multi on
EOF

# --- Enable fail2ban
sudo mkdir /etc/fail2ban
sudo touch /etc/fail2ban/jail.local
echo "[DEFAULT]"                  | sudo tee -a /etc/fail2ban/jail.local
echo "ignoreip = 127.0.0.1/8 ::1" | sudo tee -a /etc/fail2ban/jail.local
echo "bantime = 3600"             | sudo tee -a /etc/fail2ban/jail.local
echo "banaction = ufw"            | sudo tee -a /etc/fail2ban/jail.local
echo "findtime = 600"             | sudo tee -a /etc/fail2ban/jail.local
echo "maxretry = 5"               | sudo tee -a /etc/fail2ban/jail.local
echo ""                           | sudo tee -a /etc/fail2ban/jail.local
echo "[sshd]"                     | sudo tee -a /etc/fail2ban/jail.local
echo "enabled = true"             | sudo tee -a /etc/fail2ban/jail.local
sudo mkdir /var/log/fail2ban/

sudo mkdir -p /etc/systemd/system/fail2ban.service.d
sudo touch /etc/systemd/system/fail2ban.service.d/override.conf
echo "[Service]"                                                                          | sudo tee -a /etc/systemd/system/fail2ban.service.d/override.conf
echo "PrivateDevices=yes"                                                                 | sudo tee -a /etc/systemd/system/fail2ban.service.d/override.conf
echo "PrivateTmp=yes"                                                                     | sudo tee -a /etc/systemd/system/fail2ban.service.d/override.conf
echo "ProtectHome=read-only"                                                              | sudo tee -a /etc/systemd/system/fail2ban.service.d/override.conf
echo "ProtectSystem=strict"                                                               | sudo tee -a /etc/systemd/system/fail2ban.service.d/override.conf
echo "ReadWritePaths=-/var/run/fail2ban"                                                  | sudo tee -a /etc/systemd/system/fail2ban.service.d/override.conf
echo "ReadWritePaths=-/var/lib/fail2ban"                                                  | sudo tee -a /etc/systemd/system/fail2ban.service.d/override.conf
echo "ReadWritePaths=-/var/log/fail2ban"                                                  | sudo tee -a /etc/systemd/system/fail2ban.service.d/override.conf
echo "ReadWritePaths=-/var/spool/postfix/maildrop"                                        | sudo tee -a /etc/systemd/system/fail2ban.service.d/override.conf
echo "ReadWritePaths=-/run/xtables.lock"                                                  | sudo tee -a /etc/systemd/system/fail2ban.service.d/override.conf
echo "CapabilityBoundingSet=CAP_AUDIT_READ CAP_DAC_READ_SEARCH CAP_NET_ADMIN CAP_NET_RAW" | sudo tee -a /etc/systemd/system/fail2ban.service.d/override.conf

sudo mkdir -p /etc/fail2ban/jail.d/
sudo touch /etc/fail2ban/jail.d/sshd.local
echo "[sshd]"                  | sudo tee -a /etc/fail2ban/jail.d/sshd.local
echo "enabled   = true"        | sudo tee -a /etc/fail2ban/jail.d/sshd.local
echo "filter    = sshd"        | sudo tee -a /etc/fail2ban/jail.d/sshd.local
echo "banaction = iptables"    | sudo tee -a /etc/fail2ban/jail.d/sshd.local
echo "backend   = systemd"     | sudo tee -a /etc/fail2ban/jail.d/sshd.local
echo "maxretry  = 5"           | sudo tee -a /etc/fail2ban/jail.d/sshd.local
echo "findtime  = 1d"          | sudo tee -a /etc/fail2ban/jail.d/sshd.local
echo "bantime   = 2w"          | sudo tee -a /etc/fail2ban/jail.d/sshd.local
echo "ignoreip  = 127.0.0.1/8" | sudo tee -a /etc/fail2ban/jail.d/sshd.local
sudo systemctl enable fail2ban
sudo systemctl start fail2ban
curl https://download.argon40.com/argon1.sh |bash
######Local Gitlab-CE-Config
## Step1 - install Docker and Portainer
curl -fsSL https://get.docker.com -o get-docker.sh
sudo sh get-docker.sh
sudo usermod -aG docker $USER
sudo docker volume create portainer_data
sudo docker run -d -p 8000:8000 -p 9000:9000 --name=portainer --restart=always -v /var/run/docker.sock:/var/run/docker.sock -v portainer_data:/data portainer/portainer-ce:latest
sudo docker run -d -it --name watchtower -v /var/run/docker.sock:/var/run/docker.sock containrrr/watchtower:latest -e TZ Europe\Berlin --cleanup --include-restarting --include-stopped -i 3600 --rolling-restart
### Step 2 - Install Gitlab CEsudo apt-get install curl openssh-server ca-certificates apt-transport-https perl
sudo apt-get install curl openssh-server ca-certificates apt-transport-https perl
curl https://packages.gitlab.com/gpg.key | sudo tee /etc/apt/trusted.gpg.d/gitlab.asc
sudo curl -sS https://packages.gitlab.com/install/repositories/gitlab/raspberry-pi2/script.deb.sh | sudo bash
sudo apt update
sudo EXTERNAL_URL="https://gitlab.leisstefan.ddnss.eu" apt-get install gitlab-ce
echo "letsencrypt['enable'] = false" | sudo tee -a /etc/gitlab/gitlab.rb
sudo mkdir -p /etc/gitlab/ssl
sudo chmod 755 /etc/gitlab/ssl
echo "nginx['redirect_http_to_https'] = true" | sudo tee -a /etc/gitlab/gitlab.rb
#sudo openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout /etc/gitlab/ssl/gitlabserver.internal.key -out /etc/gitlab/ssl/gitlabserver.internal.crt
sudo gitlab-ctl reconfigure

## Additions
cargo install exa bat topgrade cargo-update 
argonone-config
sudo reboot 







