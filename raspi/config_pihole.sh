#!/usr/bin/env bash

######## Konfigurationsskript für Pi-Hole #########
### Pi-Hole Install and Config ###
sudo apt update && sudo apt -y dist-upgrade
curl -sSL https://install.Pi-hole.net | bash
echo "Enter IP-Adress of Pi-Hole:"
read IP
sudo mkdir /etc/dnsmasq.d/ && sudo touch /etc/dnsmasq.d/99-my-config.conf
echo listen-address=::1,127.0.0.1,${IP}| sudo tee -a /etc/dnsmasq.d/99-my-config.conf
passwd
pihole -a -p
#### Stubby Install and Config ####
sudo apt install stubby dnsutils knot-dnsutils curl vim -y
sudo systemctl stop stubby
sudo sed -i '14iRestart=on-failure' /lib/systemd/system/stubby.service
sudo sed -i '15iRestartSec=1'  /lib/systemd/system/stubby.service
sudo sed -i 's/edns_client_subnet_private : 0/edns_client_subnet_private : 1/g' /etc/stubby/stubby.yml
sudo sed -i 's/round_robin_upstreams : 0/round_robin_upstreams: 1/g' /etc/stubby/stubby.yml
sudo sed -i 's/- 127.0.0.1/- 127.0.2.2@10053/g' /etc/stubby/stubby.yml
sudo sed -i "s/- ::1/- ::1@10053/" /etc/stubby/stubby.yml
ls /usr/share/dns/root.key
sudo systemctl daemon-reload 
sudo systemctl restart stubby
dig @127.0.2.2 -p 10053 ct.de edns-client-sub.net TXT +short
sudo systemctl restart stubby
kdig @dns2.digitalcourage.de ct.de +tls
echo | openssl s_client -connect 'dns.digitalcourage.de:853' 2>/dev/null | openssl x509 -pubkey -noout | openssl pkey -pubin -outform der | openssl dgst -sha256 -binary | openssl enc -base64

#### DNSCrypt-Proxy Setup and Config ####
wget https://github.com/DNSCrypt/dnscrypt-proxy/releases/download/2.0.45/dnscrypt-proxy-linux_arm-2.0.45.tar.gz
tar xzvf dnscrypt-proxy-linux_arm-2.0.45.tar.gz
mv linux-arm dnscrypt-proxy
mv dnscrypt-proxy/example-dnscrypt-proxy.toml dnscrypt-proxy/dnscrypt-proxy.toml
sudo sed -i 's/127.0.0.1:53/127.0.2.3:10053/' dnscrypt-proxy/dnscrypt-proxy.toml
sudo sed -i "s/require_dnssec = false/require_dnssec = true/" dnscrypt-proxy/dnscrypt-proxy.toml
sudo sed -i "s/# lb_strategy/lb_strategy/" dnscrypt-proxy/dnscrypt-proxy.toml
sudo mv dnscrypt-proxy /opt/dnscrypt-proxy

## Enable auto-unattended updates
sudo apt-get install unattended-upgrades mailutils bsd-mailx apt-config-auto-update -y
echo "# manual entry" | sudo tee -a /etc/apt/apt.conf.d/50unattended-upgrades
echo "Unattended-Upgrade::Mail "stefanleis123@gmail.com";" | sudo tee -a /etc/apt/apt.conf.d/50unattended-upgrades
echo "Unattended-Upgrade::Automatic-Reboot "true";" | sudo tee -a /etc/apt/apt.conf.d/50unattended-upgrades
echo "APT::Periodic::Update-Package-Lists "1";" | sudo tee -a sudo nano /etc/apt/apt.conf.d/20auto-upgrades
echo "APT::Periodic::Download-Upgradeable-Packages "1";" | sudo tee -a sudo nano /etc/apt/apt.conf.d/20auto-upgrades
echo "APT::Periodic::Unattended-Upgrade "1";" | sudo tee -a sudo nano /etc/apt/apt.conf.d/20auto-upgrades
echo "APT::Periodic::Verbose "1";" | sudo tee -a sudo nano /etc/apt/apt.conf.d/20auto-upgrades
echo "APT::Periodic::AutocleanInterval "7";" | sudo tee -a sudo nano /etc/apt/apt.conf.d/20auto-upgrades
sudo unattended-upgrade -d -v --dry-run
sudo dpkg-reconfigure --priority=low unattended-upgrades
echo "# Pi-hole: Auto-Update Pi-hole!" | sudo tee -a /etc/cron.d/pihole
echo "30 2    * * 7    root    PATH="$PATH:/usr/local/bin/" pihole updatePihole" | sudo tee -a /etc/cron.d/pihole
sudo service cron restart
