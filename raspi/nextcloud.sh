#!/usr/bin/bash

sudo apt update && sudo apt -y dist-upgrade
sudo apt-get install unattended-upgrades mailutils bsd-mailx apt-config-auto-update -y
echo "# manual entry" | sudo tee -a /etc/apt/apt.conf.d/50unattended-upgrades
echo "Unattended-Upgrade::Mail "stefanleis123@gmail.com";" | sudo tee -a /etc/apt/apt.conf.d/50unattended-upgrades
echo "Unattended-Upgrade::Automatic-Reboot "true";" | sudo tee -a /etc/apt/apt.conf.d/50unattended-upgrades
echo "APT::Periodic::Update-Package-Lists "1";" | sudo tee -a sudo nano /etc/apt/apt.conf.d/20auto-upgrades
echo "APT::Periodic::Download-Upgradeable-Packages "1";" | sudo tee -a sudo nano /etc/apt/apt.conf.d/20auto-upgrades
echo "APT::Periodic::Unattended-Upgrade "1";" | sudo tee -a sudo nano /etc/apt/apt.conf.d/20auto-upgrades
echo "APT::Periodic::Verbose "1";" | sudo tee -a sudo nano /etc/apt/apt.conf.d/20auto-upgrades
echo "APT::Periodic::AutocleanInterval "7";" | sudo tee -a sudo nano /etc/apt/apt.conf.d/20auto-upgrades
sudo unattended-upgrade -d -v --dry-run
sudo dpkg-reconfigure --priority=low unattended-upgrades
sudo apt update
sudo apt install -y python3-pip cargo neofetch zsh zsh-syntax-highlighting autojump zsh-autosuggestions terminator hddtemp lm-sensors tldr software-properties-common ufw fail2ban vim vim-doc exa

flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
cd $HOME
touch "$HOME/.cache/zshhistory"
#-- Setup Alias in $HOME/zsh/aliasrc
git clone --depth=1 https://github.com/romkatv/powerlevel10k.git ~/powerlevel10k
echo 'source ~/powerlevel10k/powerlevel10k.zsh-theme' >> ~/.zshrc
sudo usermod -s /bin/zsh $USER
pip3 install bpytop
wget https://github.com/sharkdp/bat/releases/download/v0.18.0/bat_0.18.0_armhf.deb
sudo dpkg -i bat_0.18.0_armhf.deb
git clone --depth=1 https://gitlab.com/stefanleis1/autoinstall/qtile.git ~/qtile
git clone --depth=1 https://github.com/romkatv/powerlevel10k.git ~/powerlevel10k
echo 'source ~/powerlevel10k/powerlevel10k.zsh-theme' >> ~/.zshrc
sudo  usermod -s /bin/zsh ${USER}
mv /home/${USER}/.zshrc /home/${USER}/.zshrc.latest
cp /home/${USER}/qtile/.zshrc /home/${USER}/.zshrc
mv /home/${USER}/.bashrc /home/${USER}/.bashrc.latest
cp /home/${USER}/qtile/.bashrc /home/${USER}/.bashrc
echo "export PATH=$PATH:~/.local/bin:~/.cargo/bin" >> $HOME/.zshrc
echo "export PATH=$PATH:~/.local/bin:~/.cargo/bin" >> $HOME/.bashrc
# check for unsigned kernel modules
for mod in $(lsmod | tail -n +2 | cut -d' ' -f1); do modinfo ${mod} | grep -q "signature" || echo "no signature for module: ${mod}" ; done

#-----------------------
#--Required Packages-
#-ufw
#-fail2ban

#-start firewall
sudo systemctl enable ufw

# --- Setup UFW rules
sudo ufw limit 22/tcp  
sudo ufw allow 80/tcp  
sudo ufw allow 443/tcp  
sudo ufw default deny incoming  
sudo ufw default allow outgoing
sudo ufw enable

# --- Harden /etc/sysctl.conf
#sudo sysctl kernel.modules_disabled=1
sudo sysctl -a
sudo sysctl -A
sudo sysctl mib
sudo sysctl net.ipv4.conf.all.rp_filter
sudo sysctl -a --pattern 'net.ipv4.conf.(eth|wlan)0.arp'

# --- PREVENT IP SPOOFS
cat <<EOF > /etc/host.conf
order bind,hosts
multi on
EOF

# --- Enable fail2ban
sudo touch /etc/fail2ban/jail.local
echo "[DEFAULT]"                  | sudo tee -a /etc/fail2ban/jail.local
echo "ignoreip = 127.0.0.1/8 ::1" | sudo tee -a /etc/fail2ban/jail.local
echo "bantime = 3600"             | sudo tee -a /etc/fail2ban/jail.local
echo "banaction = ufw"            | sudo tee -a /etc/fail2ban/jail.local
echo "findtime = 600"             | sudo tee -a /etc/fail2ban/jail.local
echo "maxretry = 5"               | sudo tee -a /etc/fail2ban/jail.local
echo ""                           | sudo tee -a /etc/fail2ban/jail.local
echo "[sshd]"                     | sudo tee -a /etc/fail2ban/jail.local
echo "enabled = true"             | sudo tee -a /etc/fail2ban/jail.local
sudo mkdir /var/log/fail2ban/

sudo mkdir /etc/systemd/system/fail2ban.service.d
sudo touch /etc/systemd/system/fail2ban.service.d/override.conf
echo "[Service]"                                                                          | sudo tee -a /etc/systemd/system/fail2ban.service.d/override.conf
echo "PrivateDevices=yes"                                                                 | sudo tee -a /etc/systemd/system/fail2ban.service.d/override.conf
echo "PrivateTmp=yes"                                                                     | sudo tee -a /etc/systemd/system/fail2ban.service.d/override.conf
echo "ProtectHome=read-only"                                                              | sudo tee -a /etc/systemd/system/fail2ban.service.d/override.conf
echo "ProtectSystem=strict"                                                               | sudo tee -a /etc/systemd/system/fail2ban.service.d/override.conf
echo "ReadWritePaths=-/var/run/fail2ban"                                                  | sudo tee -a /etc/systemd/system/fail2ban.service.d/override.conf
echo "ReadWritePaths=-/var/lib/fail2ban"                                                  | sudo tee -a /etc/systemd/system/fail2ban.service.d/override.conf
echo "ReadWritePaths=-/var/log/fail2ban"                                                  | sudo tee -a /etc/systemd/system/fail2ban.service.d/override.conf
echo "ReadWritePaths=-/var/spool/postfix/maildrop"                                        | sudo tee -a /etc/systemd/system/fail2ban.service.d/override.conf
echo "ReadWritePaths=-/run/xtables.lock"                                                  | sudo tee -a /etc/systemd/system/fail2ban.service.d/override.conf
echo "CapabilityBoundingSet=CAP_AUDIT_READ CAP_DAC_READ_SEARCH CAP_NET_ADMIN CAP_NET_RAW" | sudo tee -a /etc/systemd/system/fail2ban.service.d/override.conf

sudo touch /etc/fail2ban/jail.d/sshd.local
echo "[sshd]"                  | sudo tee -a /etc/fail2ban/jail.d/sshd.local
echo "enabled   = true"        | sudo tee -a /etc/fail2ban/jail.d/sshd.local
echo "filter    = sshd"        | sudo tee -a /etc/fail2ban/jail.d/sshd.local
echo "banaction = iptables"    | sudo tee -a /etc/fail2ban/jail.d/sshd.local
echo "backend   = systemd"     | sudo tee -a /etc/fail2ban/jail.d/sshd.local
echo "maxretry  = 5"           | sudo tee -a /etc/fail2ban/jail.d/sshd.local
echo "findtime  = 1d"          | sudo tee -a /etc/fail2ban/jail.d/sshd.local
echo "bantime   = 2w"          | sudo tee -a /etc/fail2ban/jail.d/sshd.local
echo "ignoreip  = 127.0.0.1/8" | sudo tee -a /etc/fail2ban/jail.d/sshd.local

sudo systemctl enable fail2ban
sudo systemctl start fail2ban

echo "listening ports"
sudo netstat -tunlp

## setting up telegraf on server - https://portal.influxdata.com/downloads/
sudo apt-get update && sudo apt-get install apt-transport-https
wget -qO- https://repos.influxdata.com/influxdb.key | sudo apt-key add -  
echo "deb https://repos.influxdata.com/debian $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/influxdb.list

## setting up telegraf on server - https://portal.influxdata.com/downloads/
sudo apt-get update && sudo apt-get install telegraf
sudo systemctl start telegraf
telegraf config > telegraf.conf
sudo usermod -G video telegraf

echo "#In order to monitor both Network interfaces, eth0 and wlan0, uncomment, or add the next:" | sudo tee -a /etc/telegraf/telegraf.conf
echo "[[inputs.net]]" | sudo tee -a /etc/telegraf/telegraf.conf
echo "" | sudo tee -a /etc/telegraf/telegraf.conf
echo "[[inputs.netstat]]" | sudo tee -a /etc/telegraf/telegraf.conf
echo "" | sudo tee -a /etc/telegraf/telegraf.conf
echo "[[inputs.file]]" | sudo tee -a /etc/telegraf/telegraf.conf
echo  "files = ["/sys/class/thermal/thermal_zone0/temp"]" | sudo tee -a /etc/telegraf/telegraf.conf
echo  '"name_override = "cpu_temperature"' | sudo tee -a /etc/telegraf/telegraf.conf
echo  '"data_format = "value"' | sudo tee -a /etc/telegraf/telegraf.conf
echo  '"data_type = "integer"' | sudo tee -a /etc/telegraf/telegraf.conf
echo "" | sudo tee -a /etc/telegraf/telegraf.conf
echo [[inputs.exec]]"" | sudo tee -a /etc/telegraf/telegraf.conf
echo "commands = ["/opt/vc/bin/vcgencmd measure_temp"]" | sudo tee -a /etc/telegraf/telegraf.conf
echo  '"name_override = "gpu_temperature"' | sudo tee -a /etc/telegraf/telegraf.conf
echo  '"data_format = "grok"' | sudo tee -a /etc/telegraf/telegraf.conf
echo  "grok_patterns = ["%{NUMBER:value:float}"]" | sudo tee -a /etc/telegraf/telegraf.conf

## Nextcloud-Install
sudo apt install snapd
sudo snap install nextcloud
